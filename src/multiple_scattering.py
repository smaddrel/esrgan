"""Multiple Scattering v 2.0."""

import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
import os
from scipy.ndimage import gaussian_filter

import optparse

parser = optparse.OptionParser()
parser.add_option('--remote',
                  dest='remote',
                  action='store_true',
                  default=False,
                  help='Is the code being run locally or on BC4?',)
parser.add_option('--mask',
                  dest='mask',
                  action='store_true',
                  default=False,
                  help='Place a mask over the tracks randomly.',)
parser.add_option('--smear',
                  dest='smear',
                  action='store_true',
                  default=False,
                  help='Smear the saved tracks.',)
parser.add_option('--y_flag',
                  dest='y_flag',
                  action='store_true',
                  default=False,
                  help='Initial dispalcement in y.',)
parser.add_option('--n_generate',
                  dest='n_generate',
                  action='store',
                  default=200,
                  help='Enter the number of images to generate',)
parser.add_option('--n_tracks',
                  dest='n_tracks',
                  action='store',
                  default=3,
                  help='Enter the maximum number of tracks to generate',)

(options, args) = parser.parse_args()
REMOTE = options.remote
NUMBER_TO_GENERATE = int(options.n_generate)
MASK = options.mask
N_TRACKS = int(options.n_tracks)
SMEAR = options.smear
Y_FLAG = options.y_flag

A = 28  # atomic charge number
Z = 14  # atomic mass number
z = 14  # Charge number
x = 600E-6  # Thickness of the material traversed - 300 \mu m
# x = 600E-5  # Thickness of the material traversed - 300 \mu m
beta = 0.9  # Velocty in natural units
c = 3E8  # Speed of light
momentum = 3000  # Momentum of particle in MeV (?)
# X_0 is the radiation length  of the material
X_0 = 716.4*A / ((Z*(Z + 1) * np.log(287/np.sqrt(Z))))
# theta_0 is the solid angle that contains 98% of the scattering
# In this region can be treated as Gaussian -> really Moliere distribution
theta_0 = 13.6/(beta*c*momentum) * z*np.sqrt(x/X_0) * (1 + 0.038*np.log(x/X_0))
print("Computed Si radiation length is: ", X_0, 'g/cm-2? \n\n')


"""# Functions to allow scattering."""


def z_i():
    """Define a short cut for a random normal."""
    return np.random.normal()


def get_scattering():
    """Model the scattering as a gaussian distribution."""
    # http://pdg.lbl.gov/2005/reviews/passagerpp.pdf
    # eq.27.16, 27.17
    z1 = z_i()
    z2 = z_i()

    y_plane = z1*x*theta_0/np.sqrt(12) + z2*x*theta_0/2
    theta_plane = z2*theta_0

    return y_plane, theta_plane


def display_detector(positions, electrons):
    """Plot the detector layers and display the first 100 tracks."""
    electrons = [item for sublist in electrons for item in sublist]
    for p in positions:
        plt.axvline(p, linewidth=1.5, alpha=0.6)
    for e in electrons[:100]:
        plt.plot([0]+positions, e.y_history, c='r', linewidth=1, alpha=0.35)
    plt.ylim(ymax=6.4E-12, ymin=-6.4E-12)
    # plt.title("Coulomb scattering in detector")
    plt.xticks([])
    plt.yticks([])
    plt.savefig('../data/_git_images/detector.png')
    plt.close()
    if REMOTE is False:

        plt.rc('text', usetex=True)
        plt.rcParams['savefig.dpi'] = 300
        plt.rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
        plt.rcParams.update({'font.size': 15})
        plt.rcParams['text.latex.preamble'] = [
                             r'\usepackage{amsmath}',
                             r'\usepackage{amssymb}',
                             r'\usepackage{slashed}']
        plt.style.use('dark_background')
        for ip, p in enumerate(positions):
            if ip < 4:
                detector_h = (0.3, 0.7)
            elif ip < 8:
                detector_h = (0.2, 0.8)
            elif ip < 12:
                detector_h = (0.1, 0.9)
            else:
                detector_h = (0.0, 1.0)

            plt.axvline(p, linewidth=0.5, alpha=0.25, ymin=detector_h[0], ymax=detector_h[1])
        for e in electrons[:100]:
            plt.plot([0]+positions, e.y_history, c='r', linewidth=0.25, alpha=0.24)
        plt.ylim(ymax=6.4E-12, ymin=-6.4E-12)
        # plt.title("Coulomb scattering in detector")
        plt.xticks([])
        plt.yticks([])
        plt.axis('off')
        plt.figtext(0.05, 0.8, r'$\slashed{e}SRGAN$', color='white', alpha=0.6)
        plt.savefig('../data/_git_images/pretty_detector.png', dpi=600)
        plt.savefig('../data/_git_images/pretty_detector.pdf')
        plt.close()


# --------- Class Definitions ----------
class particle(object):
    """docstring for particle."""

    def __init__(self, theta=0, y=0, momentum=3000):
        """Init the particle properites."""
        super(particle, self).__init__()
        self.theta = theta
        self.y = y
        self.theta_0 = theta
        self.y_0 = y
        self.momentum = momentum

        # The locations of the hits in the layers
        self.y_history = [self.y]
        self.y_history_truth = [self.y]

        # Propagation between each layer tracked granularly
        self.full_history = [self.y]
        self.full_truth_history = [self.y]

    def generate_history(self, dx, is_base=True):
        """Wrap for the propagate method, select full / layers only."""
        if is_base is True:
            y_shift = self.propagate(dx, self.y_history,
                                     self.y_history_truth)
        else:
            y_shift = self.propagate(dx, self.full_history,
                                     self.full_truth_history)
        return y_shift

    def propagate(self, d_x, history, history_truth):
        """Step the electron to the next layer."""
        """
        The propagate method is general, with a flag to set where the
        positions are saved this will allow granular resolution as well.
        """
        y_shift = np.tan(self.theta) * (d_x)
        history.append(self.y + y_shift)
        """ Then the true track based on original values."""
        y_shift_prime = np.tan(self.theta_0) * (d_x)
        # print(self.theta_0, y_shift_prime)
        history_truth.append(self.y_0 + y_shift_prime)
        return y_shift, y_shift_prime



# Wrap this in a detector class, inheret from --> subclassing?
class sensor_module(object):
    """docstring for sensor_module."""

    def __init__(self, displacement):
        """Init a generic sensor of 4 layers at some displacement."""
        super(sensor_module, self).__init__()
        self.displacement = displacement
        self.t0 = 0
        self.t1 = 2E-2
        self.t2 = 4E-2
        self.t3 = 6E-2
        self.thickness = 250E-6

    def get_location(self, ti):
        """Dynamically get the location of the layer from a string."""
        return getattr(self, ti) + self.displacement


"""
Set the detector planes / geometry here:
- 4 layers
- each layer starts at increments of 0.5 m
- each layer is described by the sensor_module as four active layers
- each active layer starts at 2cm increments, and is 250 \mu m thick
"""
T0 = sensor_module(displacement=0.0)
T1 = sensor_module(displacement=0.5)
T2 = sensor_module(displacement=1.0)
T3 = sensor_module(displacement=1.5)
modules = [T0, T1, T2, T3]
positions = [mod.get_location('t'+str(i)) for mod in modules for i in range(4)]


def generate_track(y_flag=False):
    """Propagate a track through the detector, scattering at each layer."""
    if y_flag is False:
        y_start = 0
    else:
        # Initalise a random small displacement
        y_start = np.random.randn() * 0.4e-12
    theta_start = np.random.randn() * 0.75e-12  # 0.
    # theta_start = 0
    electron = particle(y=y_start, theta=theta_start)
    """
    1. Based on the distance between previous and current layer calculate the
       expected shift using the propagate method to get current position
    2. Then get the scattering in the current layer
    3. Update the elctron class variables and update last position
    """
    last_position = 0
    for layer_id, position in enumerate(positions):
        # Propagate the electron
        d_x = position - last_position
        y_shift, y_shift_prime = electron.generate_history(d_x, is_base=True)
        # Then scatter in the layer
        """Temp solution for now to avoid scattering in first 1x modules."""
        if layer_id > 4:
            d_y, d_theta = get_scattering()
        else:
            d_y = 0
            d_theta = 0
        electron.y += y_shift + d_y
        electron.y_0 += y_shift_prime
        electron.theta += d_theta
        last_position = position
    return electron


def save_track(positions, particle_history, path, filename, smear=True, img_size=256):
    """Save the track as a png 256x256 with interpolation between points.

    Inputs:
        positions - list of locations for each of the layers of detector
        particle_history - list of the hit points, either truth or coulomb
        --> The first two args take a list of histories for multiple tracks
        path - base path to save the files to
        filename - the actual file name structure

    NB: The size of the image is not correct to detector pixel scale yet

    Interpolation to connect the points of the tracks
    """
    img_size = img_size + 1  # Add one extra to get the right number of edges
    full_arr = np.histogram2d([], [], bins=[np.linspace(-1E-11, 1E-11, img_size),
                                            np.linspace(0, max(positions[0]),
                                            img_size)])[0]
    for pos, hist in zip(positions, particle_history):
        xnew = np.linspace(0, max(pos), img_size)
        f = interp1d(pos, hist)
        H, _, _ = np.histogram2d(f(xnew), xnew,
                                 bins=[np.linspace(-1E-11, 1E-11, img_size),
                                       np.linspace(0, max(positions[0]), img_size)])
        if MASK is True and 'input' in filename:
            x_choice = int(np.random.choice(np.linspace(80, 221, 141), 1))
            y_choice = int(np.random.choice(np.linspace(80, 221, 141), 1))
            H[x_choice: x_choice + 60, y_choice: y_choice + 60] = 0.
        full_arr += H
    H = full_arr
    H[H > 1.] = 1.  # Set the max value to 1., makes the image binary
    if SMEAR is True:
        smeared = gaussian_filter(H, sigma=0.75)
    else:
        smeared = H
    # smeared = H
    # print(np.max(smeared), 'a')
    smeared = (smeared/np.max(smeared))*255.
    # smeared = smeared.reshape(-1, 3)
    # print(np.max(smeared), 'b')
    plt.imsave(path + filename + '.png', smeared.astype(np.uint8))
    # plt.imsave(path + filename + '.png', smeared)
    plt.close()


Electrons = []
delta_ys_distribution = []
TRAIN_N = int(NUMBER_TO_GENERATE * 9/10.)
for i in range(NUMBER_TO_GENERATE):
    perc = 100 * i/NUMBER_TO_GENERATE
    print("Generating: {perc:.1f}%".
          format(perc=perc), end="\r", flush=True)
    if i < TRAIN_N:
        path = '../data/train/'
        file_type = 'train'
    else:
        path = '../data/test/'
        file_type = 'test'
    if not os.path.exists(path):
        os.makedirs(path)

    filename = '{}_input_{}'.format(file_type, i)
    N_gen = np.random.choice([t for t in range(1, N_TRACKS+1)])
    electrons = [generate_track(Y_FLAG) for track_n in range(N_gen)]
    reconstructed = [e.y_history[1:] for e in electrons]
    truth = [e.y_history_truth[1:] for e in electrons]
    x_log = [positions for track_n in range(N_TRACKS)]
    save_track(x_log, reconstructed, path, filename, smear=True)
    filename = '{}_truth_{}'.format(file_type, i)
    save_track(x_log, truth, path, filename, smear=True, img_size=512)
    Electrons.append(electrons)

print("All tracks generated")

# ------------------
display_detector(positions, Electrons[:])
