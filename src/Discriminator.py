"""Discriminator.py - holds the two classes of Discriminator."""
import tensorflow as tf
from utils import downsample


class Discriminator_(tf.keras.Model):
    """Discriminator Class sub-classing from the tf.keras.Module."""

    def __init__(self,):
        """Define the layers used in the network."""
        super(Discriminator_, self).__init__()
        initializer = tf.random_normal_initializer(0., 0.02)
        self.down1 = downsample(64, 4, False)
        self.down2 = downsample(128, 4)
        self.down3 = downsample(256, 4)
        self.zero_pad1 = tf.keras.layers.ZeroPadding2D()
        self.conv = tf.keras.layers.Conv2D(512, 4, strides=1,
                                           kernel_initializer=initializer,
                                           use_bias=False)
        self.batchnorm1 = tf.keras.layers.BatchNormalization()
        self.leaky_relu = tf.keras.layers.LeakyReLU()
        self.zero_pad2 = tf.keras.layers.ZeroPadding2D()
        self.last = tf.keras.layers.Conv2D(1, 4, strides=1,
                                           kernel_initializer=initializer)

    def call(self, inputs, target, training=False):
        """Calculate the forward pass through the network."""
        x = tf.keras.layers.concatenate([inputs, inputs])
        # x = tf.keras.layers.concatenate([inputs, target])
        # x = input
        x = self.down1(x)
        x = self.down2(x)
        x = self.down3(x)
        x = self.zero_pad1(x)
        x = self.conv(x)
        x = self.batchnorm1(x)
        x = self.leaky_relu(x)
        x = self.zero_pad2(x)
        x = self.last(x)
        # print("D", x.shape)
        return x
# -----------------------------


class simple_Discriminator(tf.keras.Model):
    """docstring for simple_Discriminator."""

    def __init__(self, OUTPUT_CHANNELS):
        """Define the layers to be used in the model."""
        super(simple_Discriminator, self).__init__()
        self.conv_2d_1 = tf.keras.layers.Conv2D(64, (5, 5), strides=(2, 2),
                                                padding='same',
                                                input_shape=[256, 256,
                                                OUTPUT_CHANNELS])
        self.conv_2d_2 = tf.keras.layers.Conv2D(128, (5, 5), strides=(2, 2),
                                                padding='same')

        self.leaky_relu = tf.keras.layers.LeakyReLU()
        self.dropout = tf.keras.layers.Dropout(0.3)
        self.dense = tf.keras.layers.Dense(1)

    def call(self, input, training=False):
        """Calculate the forward pass through the network."""
        x = self.conv_2d_1(input)
        x = self.leaky_relu(x)
        x = self.dropout(x, training=training)

        x = self.conv_2d_2(input)
        x = self.leaky_relu(x)
        x = self.dropout(x, training=training)

        x = tf.keras.layers.Flatten()(x)
        x = self.dense(x)
        return x
