"""plotting.py - collection of plotting methods."""
import os
import matplotlib.pyplot as plt
import tensorflow as tf
import numpy as np


def generate_images(model, test_input, tar, img_size=256, **kwargs):
    """Generate image from input, compare with target."""
    # base_path='', epoch=0,
    base_path = kwargs['base_path']
    epoch = kwargs['epoch']
    OUTPUT_CHANNELS = kwargs['OUTPUT_CHANNELS']
    # OUTPUT_CHANNELS=3):
    # For simplicity this manages directories.
    if not os.path.exists(base_path):
        os.makedirs(base_path)

    # Make prediction on Model
    # This here is the inference seciton to time
    start = tf.timestamp()
    prediction = model(test_input, training=False)
    end = tf.timestamp()

    # plt.figure(figsize=(15, 8))
    f, ax = plt.subplots(nrows=1, ncols=3, figsize=(15, 8))
    # option to automatically shape correctly for 1D or 3D colour channels
    if OUTPUT_CHANNELS == 1:
        display_list = [tf.reshape(test_input, [256, 256]),
                        tf.reshape(tar, [256, 256]),
                        tf.reshape(prediction, [256, 256])]
    else:
        display_list = [tf.reshape(test_input, [256, 256, OUTPUT_CHANNELS]),
                        tf.reshape(tar, [img_size, img_size, OUTPUT_CHANNELS]),
                        tf.reshape(prediction, [img_size, img_size, OUTPUT_CHANNELS])]
    title = ['Input Image', 'Ground Truth', 'Predicted Image']
    for i in range(3):
        ax[i].set_title(title[i])
        ax[i].imshow(display_list[i] * 0.5 + 0.5)

        # ax[i].axis('off')
    plt.savefig(base_path + '/result_{}.png'.format(epoch), dip=2000)
    plt.close()
    return tf.Variable(end-start)


def save_disc_output(model, discrim, test_input, tar, base_path, epoch):
    """Save the output from PatchGAN discriminator."""
    # generator, discriminator, inp_test, tar_test,
    if not os.path.exists(base_path):
        os.makedirs(base_path)
    prediction = model(test_input, training=True)
    decision_fake = discrim(test_input, prediction, training=True)
    # decision_fake = discrim(prediction, training=True) inputs, target
    decision_true = discrim(test_input, tar, training=True)
    plt.figure(figsize=(15, 8))
    display_list = [decision_fake, decision_true]
    title = ['decision fake', 'decision true', 'prediction', 'target']
    for i in range(2):
        plt.subplot(1, 2, i+1)
        plt.imshow(display_list[i][0, ..., -1], cmap='RdBu_r')
        plt.colorbar()
        plt.title(title[i])

    plt.savefig(base_path + '/decision_{}.png'.format(epoch))
    plt.close()


def plot_accuracy(losses, base_path):
    """Plot the loss and accuracy scores through training."""
    G_loss = losses[0]
    D_patch = losses[1]
    D_one = losses[2]
    l1_loss_log = losses[3]
    gan_loss_log = losses[4]
    G_loss = [g[0].numpy() for g in G_loss]
    # train_log = [g[0].numpy() for g in train_log]
    D_patch = [g[0].numpy() for g in D_patch]
    D_one = [g[0].numpy() for g in D_one]
    l1_loss_log = [g[0].numpy() for g in l1_loss_log]
    gan_loss_log = [g[0].numpy() for g in gan_loss_log]

    fig, (ax1, ax2, ax3) = plt.subplots(3, sharex=True, figsize=(10, 12))

    def smooth(y, box_pts):
        box = np.ones(box_pts)/box_pts
        y_smooth = np.convolve(y, box, mode='same')
        return y_smooth
    xs = [_ for _ in range(len(G_loss))]
    if len(xs) > 2:
        window = 1
        f, axarr = plt.subplots(3, figsize=(10, 12), sharex=True)
        # First the Patch Loss
        axarr[0].plot(D_patch, alpha=0.4, linewidth=5)
        axarr[0].plot(xs[window:-window],
                      smooth(D_patch, window)[window:-window], c='C0')
        axarr[0].set_title('Discriminator Patch')
        axarr[0].set_yscale('log')
        axarr[0].set_ylabel('loss')
        # Second the one shot loss
        axarr[1].plot(D_one, alpha=0.4, linewidth=5)
        axarr[1].plot(xs[window:-window],
                      smooth(D_one, window)[window:-window], c='C0')
        axarr[1].set_title('Discriminator One Shot')
        axarr[1].set_yscale('log')
        axarr[1].set_ylabel('loss')
        # Then the G losses
        axarr[2].plot(G_loss, alpha=0.4, linewidth=5, c='C1', label='Total')
        axarr[2].plot(xs[window:-window],
                      smooth(G_loss, window)[window:-window], c='C1')
        axarr[2].plot(l1_loss_log, alpha=0.4, linewidth=5, c='C4', label='L1')
        axarr[2].plot(xs[window:-window],
                      smooth(l1_loss_log, window)[window:-window], c='C4')
        axarr[2].plot(gan_loss_log, alpha=0.4, linewidth=5, c='C9', label='G')
        axarr[2].plot(xs[window:-window],
                      smooth(gan_loss_log, window)[window:-window], c='C9')
        axarr[2].set_title('Generator')
        axarr[2].legend()
        axarr[2].set_xlabel('Epoch')
        axarr[2].set_ylabel('loss')
        plt.tight_layout()
        plt.savefig(base_path + '/results/loss_tracking.pdf')
        plt.close()
    # ----------
