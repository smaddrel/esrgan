"""Benchmark the timings of the model on different hardware."""
import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd

import seaborn as sns


def read_file(filename):
    """Read the text file in."""
    with open('{}'.format(filename), 'r') as f:
        lines = f.readlines()
        lines = [line.strip('\n') for line in lines]
    return lines


def strip_times(line):
    """Strip the times from the line.

    Input:
        line - string, containing the line.

    Return:
        epoch_time - the epoch time in seconds
        training time - avergage trianing time
        inference time - inference subset time
    """
    line = line.split(',')
    times = [float(element.split(': ')[1]) for element in line[1:]]
    epoch_time = times[0]
    training_time = times[1]
    inference_time = times[2]
    return epoch_time, training_time, inference_time

def plot_training_vs_inference(training, inference):
    """Plot the average with errors training and inference time."""
    di = {}
    di['Train'] = training
    di['Infer'] = inference
    di['hardware'] = ['GPU']*len(training)
    df = pd.DataFrame(di)
    print(df.describe())
    print(df)
    train_ave = np.mean(training)
    inference_ave = np.mean(inference)
    train_err = np.std(training)
    inference_err = np.std(inference)


    labels = ['Train', 'Infer']
    men_means = [train_ave, inference_ave]
    women_means = [train_ave, inference_ave]
    g_yerr = [train_err, inference_err]
    i_yerr = [train_err, inference_err]

    x = np.arange(len(labels))  # the label locations
    width = 0.35  # the width of the bars


    fig, ax = plt.subplots()
    rects1 = ax.bar(x - width/2, men_means, width, yerr=g_yerr, label='GPU')
    rects2 = ax.bar(x + width/2, women_means, width, yerr=i_yerr, label='IPU')

    # Add some text for labels, title and custom x-axis tick labels, etc.
    ax.set_ylabel('Time ')
    ax.set_title('')
    ax.set_xticks(x)
    ax.set_xticklabels(labels)
    ax.legend()
    plt.show()

    fig, ax = plt.subplots(1, 2)
    ax[0].hist(training, bins=50)
    ax[1].hist(inference, bins=50)
    plt.show()



epoch_times = []
training_times = []
inference_times = []
lines = read_file('../results/base_base_path/time_tracker.dat')
for line in lines[1:]:
    epoch_time, training_time, inference_time = strip_times(line)
    print(epoch_time, training_time, inference_time)
    epoch_times.append(epoch_time)
    training_times.append(training_time)
    inference_times.append(inference_time)

plot_training_vs_inference(training_times, inference_times)
