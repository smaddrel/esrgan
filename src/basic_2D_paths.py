
"""First 2D electron path simulation, incredibly basic minimial physics."""
import numpy as np
import matplotlib.pyplot as plt
from sklearn.utils import shuffle
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import accuracy_score
from datetime import datetime
import os

NUMBER_TO_GENERATE = 0.1e4


class electron_gun(object):
    """docstring for electron_gun."""

    def __init__(self, x=32, y=32):
        """Init the basics."""
        self.x = x
        self.y = y
        self.grid = np.zeros(shape=(self.x, self.y))

    def generate_electron(self):
        """Generate an electron from a normal distribution."""
        inital_pos = (0, 0)  # x = 0, y = 7
        velocity = np.random.randn()
        theta = 0.3*np.random.randn()
        return inital_pos, velocity, theta

    def purturbation(self, truth):
        """Disturbance due to interaction."""
        r = np.random.random()
        if r < 0.2 and truth is False:
            dtheta = 0.15*np.random.randn()
        else:
            dtheta = 0
        return dtheta

    def time_step(self, inital_pos, velocity, theta, truth=False):
        """Step the electron across the tracker."""
        has_kink = False
        x = inital_pos[0]
        y = inital_pos[1]
        dtheta = 0
        history = [[inital_pos[0]], [inital_pos[1]]]
        while x < self.x - 1:
            x += 1
            if has_kink is False and truth is False and x > 10:
                dtheta = self.purturbation(truth)
                if dtheta is not 0:
                    has_kink = True
            y = y + 1*np.tan(theta + dtheta)
            history[0].append(x)
            history[1].append(y)
        return history


def save_data(data, filename):
    """Save the data as .png files, either test / train."""
    base_path = 'data/' + filename.split('_')[0]
    if not os.path.exists(base_path):
        os.makedirs(base_path)
    x_bins = np.linspace(0, 32, 33)
    y_bins = np.linspace(-16, 16, 33)
    track = []
    for d in data:
        full_arr = np.histogram2d([], [], bins=(x_bins, y_bins))[0]
        for i in range(len(d)//2):
            arr = np.histogram2d(x_bins[: -1].tolist(), d[i*2 + 1],
                                 bins=(x_bins, y_bins))[0]
            full_arr += arr
        track.append(full_arr)
    for i, plate in enumerate(track):
        plt.imsave('data/{}/{}_{}.png'.
                   format(filename.split('_')[0], filename, i), plate)


def plot_binning(tracks, truth):
    """Plot the tracks per bin as representive of real data."""
    x_bins = np.linspace(0, 28, 29)
    y_bins = np.linspace(-10, 10, 200)
    # base = np.zeros()
    # hist = np.histogram2d(tracks[1], tracks[0],  bins=(y_bins, x_bins))
    hist_tracks = []
    hist_xs = []
    for thing in tracks:
        hist_tracks += thing[0]
        hist_xs += thing[1]

    plt.figure(figsize=(14, 8))
    plt.hist2d(hist_tracks, hist_xs,  bins=(x_bins, y_bins), cmap='Blues')
    for i in range(len(truth)):
        plt.plot(truth[i][0], truth[i][1], c='C1', linewidth=1, alpha=0.4)
    plt.savefig('hist_single_track.png')
    plt.close()

def bin_tracks(tracks):
    x_bins = np.linspace(0, 17, 17)
    y_bins = np.linspace(-10, 10, 200)
    data = []
    for i in range(len(tracks)):
        hist = np.histogram2d(tracks[i][0], tracks[i][1],  bins=(x_bins, y_bins))
        data.append(hist[0])
    return data

def plot_path(tracks, color='C1', truth=True):
    plt.axvline(16, linestyle='--', color='grey', linewidth=1)
    plt.axhline(10, linestyle='--', color='grey', linewidth=1)
    plt.axhline(-10, linestyle='--', color='grey', linewidth=1)

    plt.ylim(-10.5, 10.5)
    for history in tracks:
        plt.plot(history[0], history[1], color=color, linewidth=0.7, alpha=0.4 )
        # plt.scatter(history[0], history[1], color=color, s=1.7, alpha=0.4)

def manager():
    pass


def main(n_tracks=0):
    """Set n_tracks = 0 -> normal gen, n_tracks = 1, 2, 3... specific #."""
    electron = electron_gun()
    tracks = []
    truth_tracks = []
    velocties = []
    thetas = []
    t0 = datetime.now()
    for i in range(int(NUMBER_TO_GENERATE)):
        # Where to get the single kick in?
        if i % 1000 == 0:
            t1 = datetime.now()
            print('Proccesed {} tracks, time: {}'.format(i, t1-t0))
            t0 = datetime.now()
        full_history = []
        full_truth_history = []
        if n_tracks == 0:
            if np.random.randn() > 0.7:
                gen_n_track = 2
            else: gen_n_track = 1
        else: gen_n_track = n_tracks
        for track_gen in range(gen_n_track):
            pos, v, theta = electron.generate_electron()
            velocties.append(v)
            thetas.append(theta)
            history = electron.time_step(pos, v, theta)
            truth_history = electron.time_step(pos, v, theta, truth=True)
            full_history += history
            full_truth_history += truth_history


        tracks.append(full_history)
        truth_tracks.append(full_truth_history)
    return tracks, truth_tracks


if __name__ == '__main__':
    n_tracks =  0
    tracks, truth_tracks = main(n_tracks=n_tracks)
    number_of_tracks = len(tracks) - len(tracks) // 3
    # print(tracks)
    # print(number_of_tracks)
    if n_tracks == 0:
        n_string = ''
    else:
        n_string = str(n_tracks)
    save_data(tracks[:number_of_tracks], 'train{}_input'.format(n_string))
    save_data(truth_tracks[:number_of_tracks], 'train{}_truth'.format(n_string))
    save_data(tracks[number_of_tracks:], 'test{}_input'.format(n_string))
    save_data(truth_tracks[number_of_tracks:], 'test{}_truth'.format(n_string))
    # plt.hist(velocties, bins=20)
    # plt.savefig('velocities.png')
    # plt.hist(thetas, bins=20)
    # plt.savefig('theta.png')
    # plt.close()
    # plot_path(truth_tracks, color='C0')
    # plot_path(tracks, truth=False)
    # custom_lines = [plt.Line2D([0], [0], color='C1', lw=4),
    #                 plt.Line2D([0], [0], color='C0', lw=4)]
    # plt.legend(custom_lines, ['reconstructed', 'truth'], loc='upper left')
    # plt.savefig('track_compare.png')
    # plt.close()

    # tracks_b = bin_tracks(tracks)
    # tracks = np.asarray(tracks)
    # truth_tracks = np.asarray(truth_tracks)
    # tracks = tracks.flatten(tracks)
    # tracks = truth_tracks.flatten(truth_tracks)
    plot_binning(tracks[0:5], truth_tracks[0:5])

    # classifier = basic_classifier()
    # data = classifier.parse_data()
    # classifier.GBC(data)
    # classifier.MLP(data)
