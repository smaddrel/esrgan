"""Pix2Pix in tf 1.14 - eager execution."""
import tensorflow as tf
import os
import shutil
import time
import optparse
import plotting as P


from Discriminator import Discriminator_, simple_Discriminator
from Generator import Generator_


"""
Read in command line args and set defaults:
"""
print("Starting the eSRGAN file...")
parser = optparse.OptionParser()
parser.add_option('--remote',
                  dest='remote',
                  action='store_true',
                  default=False,
                  help='Is the code being run locally or on BC4?',)
parser.add_option('--base_path',
                  dest='base_path',
                  action='store',
                  default='base_path',
                  help='Name the base path to store results and ckpts in.',)
parser.add_option('--clean',
                  dest='clean',
                  action='store_true',
                  default=False,
                  help='Clean the output directory of results / ckpts',)
parser.add_option('--train',
                  dest='train',
                  action='store_true',
                  default=False,
                  help='Clean the output directory of results / ckpts',)
parser.add_option('--n_samples',
                  dest='n_samples',
                  action='store',
                  default=2000,
                  help='Set the maximum number of samples',)
parser.add_option('--lr',
                  dest='learning_rate',
                  action='store',
                  default=2e-4,
                  help='Set the maximum number of samples',)
parser.add_option('--lambda',
                  dest='Lamb',
                  action='store',
                  default='100',
                  help='Clean the output directory of results / ckpts',)
parser.add_option('--one_shot',
                  dest='one_shot',
                  action='store_false',
                  default=True,
                  help='Have the additional Discriminator running',)
parser.add_option('--save',
                  dest='save',
                  action='store_false',
                  default=True,
                  help='save the checkpoint, to turn off on cluster testing',)
(options, args) = parser.parse_args()
print(options)
REMOTE = options.remote
BASE_PATH = 'base_'+options.base_path
CLEAN = options.clean
TRAIN = options.train
N_SAMPLES = int(options.n_samples)
ONE_SHOT = float(options.one_shot)
SAVE = options.save


# tf.enable_eager_execution()

EPOCHS = 2000
BUFFER_SIZE = 400
BATCH_SIZE = 10
IMG_WIDTH = 256
IMG_HEIGHT = 256
OUTPUT_CHANNELS = 3
LAMBDA = float(options.Lamb)  # Automatically set to 100
LEARNING_RATE = float(options.learning_rate)  # Default option: 2e-4


def load(image_file):
    """Load the raw .png file."""
    image = tf.io.read_file(image_file)
    input_image = tf.image.decode_png(image)

    input_image = tf.image.decode_png(image, channels=OUTPUT_CHANNELS)
    input_image = tf.cast(input_image, tf.float32)
    return input_image


def resize(input_image, real_image, height, width):
    """Resize the image to set size."""
    input_image =\
        tf.image.resize(input_image, [height, width],
                        method=tf.image.ResizeMethod.NEAREST_NEIGHBOR)
    real_image = tf.image.resize(real_image, [height, width],
                                 method=tf.image.ResizeMethod.NEAREST_NEIGHBOR)
    return input_image, real_image


def random_crop(input_image, real_image):
    """Random crop method to increase dataset size."""
    stacked_image = tf.stack([input_image, real_image], axis=0)
    cropped_image = tf.image.random_crop(
        stacked_image, size=[2, IMG_HEIGHT, IMG_WIDTH, 3])
    return cropped_image[0], cropped_image[1]


def normalize(input_image):
    """Normalize the images to [-1, 1]."""
    input_image = (input_image / 127.5) - 1
    return input_image


@tf.function()
def random_jitter(input_image, real_image):
    """Resizing to 286 x 286 x 3."""
    input_image, real_image = resize(input_image, real_image, 256, 256)
    return input_image, real_image


def load_image_train(image_file):
    """Load the train images and normalise the values."""
    input_image = load(image_file)
    input_image = normalize(input_image)
    return input_image


def load_image_test(image_file):
    """Load the test images and normalise the values."""
    input_image = load(image_file)
    input_image = normalize(input_image)
    return input_image


"""## Input Pipeline"""
"""
- Four datasets, one for train and one for test
- Then have one for each of the input and output
- Set the base path based on local / remote options.
"""
if REMOTE is False:
    base_path = '/Users/sm12173/Documents/'
else:
    base_path = '/mnt/storage/home/sm12173/'

if CLEAN:
    mydir = base_path + 'esrgan/results/' + BASE_PATH
    print("Cleaning directory: {}".format(mydir))
    if os.path.exists(mydir):
        print([os.path.join(mydir, f) for f in os.listdir(mydir)])

        def rm(f):
            """Remove the files / directory."""
            print(f)
            if os.path.isdir(f):
                return shutil.rmtree(f)
            if os.path.isfile(f):
                return os.unlink(f)
        for f in os.listdir(mydir):
            rm(os.path.join(mydir, f))


print("Loading datasts...")
# ---------
train_input_dataset =\
    tf.data.Dataset.list_files(base_path + 'esrgan/data/train/' +
                               'train_input_*.png', shuffle=False)
train_input_dataset = train_input_dataset.map(load_image_train)
train_input_dataset = train_input_dataset.batch(BATCH_SIZE)

train_truth_dataset =\
    tf.data.Dataset.list_files(base_path + 'esrgan/data/train/' +
                               'train_truth_*.png', shuffle=False)
train_truth_dataset = train_truth_dataset.map(load_image_train)
train_truth_dataset = train_truth_dataset.batch(BATCH_SIZE)

test_input_dataset =\
    tf.data.Dataset.list_files(base_path + 'esrgan/data/test/' +
                               'test_input_*.png', shuffle=False)
test_input_dataset = test_input_dataset.map(load_image_test)
test_input_dataset = test_input_dataset.batch(1)

test_truth_dataset =\
    tf.data.Dataset.list_files(base_path + 'esrgan/data/test/' +
                               'test_truth_*.png', shuffle=False)
test_truth_dataset = test_truth_dataset.map(load_image_test)
test_truth_dataset = test_truth_dataset.batch(1)
print("Loading done")
# for tar in test_truth_dataset:
#     import matplotlib.pyplot as plt
#     import numpy as np
#     img = tf.reshape(tar, [512, 512, OUTPUT_CHANNELS])
#     plt.imsave('result_test.png', img * 0.5 + 0.5)
#     plt.close()
#     plt.imshow(img.numpy().astype(np.float32) * 0.5 + 0.5)
#     plt.axis('off')
#     plt.savefig('result_test2.pdf')
#     plt.close()
#     break


""" Network class initializer."""
generator = Generator_(OUTPUT_CHANNELS)
discriminator = Discriminator_()
discrim_one_shot = simple_Discriminator(OUTPUT_CHANNELS)
# ---------------------------

loss_object = tf.keras.losses.BinaryCrossentropy(from_logits=True)


def discriminator_loss(disc_real_output, disc_generated_output):
    """Set the discriminator loss: E_y[log(D(y))]."""
    # Sums the D(input) loss for both fake and real images.
    real_loss = loss_object(tf.ones_like(disc_real_output), disc_real_output)
    generated_loss = loss_object(tf.zeros_like(disc_generated_output),
                                 disc_generated_output)
    total_disc_loss = real_loss + generated_loss
    return total_disc_loss


def generator_loss(disc_gen_output, disc_gen_output_one, gen_output, target):
    """Set the GAN loss: E_{x, y}[log(1 - D(G))] + lambda_1*L_1."""
    gan_loss_a = loss_object(tf.ones_like(disc_gen_output),
                             disc_gen_output)
    gan_loss_b = loss_object(tf.ones_like(disc_gen_output_one),
                             disc_gen_output_one)
    # mean absolute error
    l1_loss = tf.reduce_mean(tf.abs(target - gen_output))
    if ONE_SHOT is False:
        total_gen_loss = gan_loss_a + (LAMBDA * l1_loss)
        gan_loss = gan_loss_a
    else:
        total_gen_loss = gan_loss_a + gan_loss_b + (LAMBDA * l1_loss)
        gan_loss = gan_loss_a + gan_loss_b
    return total_gen_loss, gan_loss, LAMBDA*l1_loss


gen_optimizer = tf.keras.optimizers.Adam(LEARNING_RATE, beta_1=0.5)
disc_optimizer = tf.keras.optimizers.Adam(LEARNING_RATE, beta_1=0.5)
disc_one_optimizer = tf.keras.optimizers.Adam(1e-4)

"""## Checkpoints (Object-based saving)"""
print("Checkpoints setup.")
checkpoint_dir = '../results/{}/training_checkpoints'.format(BASE_PATH)
checkpoint_prefix = os.path.join(checkpoint_dir, "ckpt")
checkpt = tf.train.Checkpoint(generator_optimizer=gen_optimizer,
                              discriminator_optimizer=disc_optimizer,
                              generator=generator,
                              discriminator=discriminator)


def plotting_controler(generator, discriminator, test_input_dataset,
                       test_truth_dataset, epoch, *losses):
    """Plot the training losses, and the output from Gen and Disc.

    Inputs:
        generator - the generator model to produce images
        discriminator - model to get patchGAN results
        test_input_dataset - input images from test set - tf.Dataset
        test_truth_dataset - input images from truth set - tf.Dataset
        epoch - int for plotting / saving purposes
        *losses - args for the losses that feed directly into the loss plot.
    """
    inference_time = []

    g_loss_log = losses[0]
    l1_loss_log = losses[1]
    gan_loss_log = losses[2]
    d_loss_log = losses[3]
    d_one_loss_log = losses[4]
    zipped_ds = zip(test_input_dataset, test_truth_dataset)
    for i, (inp_test, tar_test) in enumerate(zipped_ds):
        inf_time = P.generate_images(generator, inp_test, tar_test,
                          base_path=base_path +
                          'esrgan/results/{}/results/{}'.
                          format(BASE_PATH, i),
                          epoch=epoch,
                          OUTPUT_CHANNELS=OUTPUT_CHANNELS,
                          img_size=512)
        inference_time.append(inf_time.numpy())
        # P.save_disc_output(generator, discriminator, inp_test, tar_test,
        #                    base_path=base_path +
        #                    'esrgan/results/{}/decision/{}'.
        #                    format(BASE_PATH, i),
        #                    epoch=epoch)
        if i >= 10:
            # Only show results for 10 images
            break

    P.plot_accuracy(losses, base_path=base_path +
                    'esrgan/results/{}/'.format(BASE_PATH))
    return inference_time


@tf.function
def train_step(input_image, target, **metrics):
    """Set up individual train step set with GradientTape."""
    # Set the metrics from the **metrics kwargs
    g_loss_metric = metrics['g_loss_metric']
    d_loss_metric = metrics['d_loss_metric']
    d_one_loss_metric = metrics['d_one_loss_metric']
    l1_loss_metric = metrics['l1_loss_metric']
    gan_loss_metric = metrics['gan_loss_metric']
    training_time_metric = metrics['training_time']
    inference_time_metric = metrics['inference_time']
    start = tf.timestamp()
    with tf.GradientTape() as gen_tape, tf.GradientTape() as disc_tape, tf.GradientTape() as disc_one_tape:
        inf_start = tf.timestamp()
        gen_output = generator(input_image, training=True)
        inf_end = tf.timestamp()
        disc_real_output = discriminator(input_image, target, training=True)
        disc_generated_output = discriminator(input_image, gen_output,
                                              training=True)
        disc_one_real_output = discrim_one_shot(target, training=True)
        disc_one_generated_output = discrim_one_shot(gen_output, training=True)
        gen_loss, gan_loss, l1_loss = generator_loss(disc_generated_output,
                                                     disc_one_generated_output,
                                                     gen_output, target)
        disc_loss = discriminator_loss(disc_real_output, disc_generated_output)
        disc_one_loss = discriminator_loss(disc_one_real_output,
                                           disc_one_generated_output)
    end = tf.timestamp()
    training_time_tensor = end - start
    inference_time_tensor = inf_end - inf_start

    # Update the metrics
    g_loss_metric.update_state(gen_loss)
    l1_loss_metric.update_state(l1_loss)
    gan_loss_metric.update_state(gan_loss)
    d_loss_metric.update_state(disc_loss)
    d_one_loss_metric.update_state(disc_one_loss)
    training_time_metric.update_state(training_time_tensor)
    inference_time_metric.update_state(inference_time_tensor)


    g_gradients = gen_tape.gradient(gen_loss, generator.trainable_variables)
    d_gradients = disc_tape.gradient(disc_loss,
                                     discriminator.trainable_variables)
    d_one_gradients = disc_one_tape.gradient(disc_one_loss,
                                     discrim_one_shot.trainable_variables)

    gen_optimizer.apply_gradients(zip(g_gradients,
                                      generator.trainable_variables))
    disc_optimizer.apply_gradients(zip(d_gradients,
                                       discriminator.trainable_variables))
    if ONE_SHOT is True:
        disc_one_optimizer.apply_gradients(zip(d_one_gradients,
                                           discrim_one_shot.trainable_variables))



def train(input_dataset, target_dataset, epochs):
    """Step through the traiing process and print monitoring metrics."""
    print("starting training")
    # Timing profile metrics
    training_time_log = []
    inference_time_log = []
    true_inference_time_log = []
    epoch_time_log = []
    # Create the metrics
    g_loss_log = []
    l1_loss_log = []
    gan_loss_log = []
    d_loss_log = []
    d_one_loss_log = []
    g_loss_metric = tf.keras.metrics.Mean(name='g_train_loss')
    l1_loss_metric = tf.keras.metrics.Mean(name='l1_train_loss')
    gan_loss_metric = tf.keras.metrics.Mean(name='gan_train_loss')
    d_loss_metric = tf.keras.metrics.Mean(name='d_train_loss')
    d_one_loss_metric = tf.keras.metrics.Mean(name='d_one_train_loss')
    training_time = tf.keras.metrics.Mean(name='training_time')
    inference_time = tf.keras.metrics.Mean(name='inference_time')

    for epoch in range(epochs):
        print("------ Epoch: ", epoch, " ------")
        start = time.time()
        # Reset the metrics
        g_loss_metric.reset_states()
        l1_loss_metric.reset_states()
        gan_loss_metric.reset_states()
        d_loss_metric.reset_states()
        d_one_loss_metric.reset_states()
        training_time.reset_states()
        inference_time.reset_states()
        counter = 0
        total_files = len(list(input_dataset))

        for count, (input_image, target) in enumerate(zip(input_dataset,
                                                          target_dataset),
                                                      start=1):
            perc = 100 * count/total_files
            if REMOTE is False:
                if count != total_files:
                    print("Done {count}/{total_files} batches: {perc:.1f}%".
                          format(count=count, total_files=total_files,
                                 perc=perc), end="\r", flush=True)
                else:
                    print("Done {count}/{total_files} batches: {perc:.1f}%".
                          format(count=count, total_files=total_files,
                                 perc=perc))
            train_step(input_image, target,
                       g_loss_metric=g_loss_metric,
                       d_loss_metric=d_loss_metric,
                       d_one_loss_metric=d_one_loss_metric,
                       l1_loss_metric=l1_loss_metric,
                       gan_loss_metric=gan_loss_metric,
                       training_time=training_time,
                       inference_time=inference_time)
            counter += 1
            if counter >= N_SAMPLES:
                # Allows arbiaty dataset to be limited from command line
                # Breaks at the number of batches.
                break
        # Get the metric results
        g_mean_loss = g_loss_metric.result()
        l1_mean_loss = l1_loss_metric.result()
        gan_mean_loss = gan_loss_metric.result()
        d_mean_loss = d_loss_metric.result()
        d_one_mean_loss = d_one_loss_metric.result()
        training_time_mean = training_time.result()
        inference_time_mean = inference_time.result()
        g_loss_log.append([g_mean_loss])
        l1_loss_log.append([l1_mean_loss])
        gan_loss_log.append([gan_mean_loss])
        d_loss_log.append([d_mean_loss])
        d_one_loss_log.append([d_one_mean_loss])
        epoch_time = time.time()-start
        epoch_time_log.append(time.time()-start)
        training_time_log.append(training_time_mean)
        inference_time_log.append(inference_time_mean)
        print('loss: G = {:.5f}, D = {:.5f}, G+D = {:.5f}, G:D = {:.5f} (D_1 = {:.5f}, l1 = {:.5f}, gan = {:.5f})'.
              format(g_mean_loss, d_mean_loss, g_mean_loss + d_mean_loss,
                     g_mean_loss/d_mean_loss, d_one_mean_loss,
                     l1_mean_loss, gan_mean_loss) +
              ', time = {:.1f} secs.'.format(time.time()-start))

        if epoch < 200:
            if (epoch) % 1 == 0:
                inf_time = plotting_controler(generator, discriminator,
                                              test_input_dataset,
                                              test_truth_dataset,
                                              epoch,
                                              g_loss_log,
                                              l1_loss_log,
                                              gan_loss_log,
                                              d_loss_log,
                                              d_one_loss_log)
                true_inference_time_log.extend(inf_time)
                # print(true_inference_time_log)


        else:
            if (epoch) % 50 == 0:
                inf_time = plotting_controler(generator, discriminator,
                                              test_input_dataset,
                                              test_truth_dataset,
                                              epoch,
                                              g_loss_log,
                                              l1_loss_log,
                                              gan_loss_log,
                                              d_loss_log,
                                              d_one_loss_log)
                true_inference_time_log.extend(inf_time)
                # print(true_inference_time_log)
        # inference_time_mean = result.inf
        epoch_time = time.time()-start
        if epoch == 0:
            # Create a file that keeps track of the loss data for tracking
            print('../results/'+ BASE_PATH +'/loss_tracker.dat')
            f = open('../results/'+ BASE_PATH +'/loss_tracker.dat', 'w')
            f.write("Loss tracking record: \n")
            f.close()
            # Create a file that keeps track of the timing for tracking
            print('../results/'+ BASE_PATH +'/time_tracker.dat')
            f_time = open('../results/'+ BASE_PATH +'/time_tracker.dat', 'w')
            f_time.write("Time tracking record: \n")
            f_time.close()
            # Create a file that keeps track of the inference timing for tracking
            print('../results/'+ BASE_PATH +'/inference_tracker.dat')
            f_inf = open('../results/'+ BASE_PATH +'/inference_tracker.dat', 'w')
            f_inf.write("Time tracking record: \n")
            f_inf.close()
        f = open('../results/'+ BASE_PATH +'/loss_tracker.dat', 'a')
        f.write('loss: G = {:.5f}, D = {:.5f}, G+D = {:.5f}, G:D = {:.5f} (D_1 = {:.5f}, l1 = {:.5f}, gan = {:.5f})'.
                format(g_mean_loss, d_mean_loss, g_mean_loss + d_mean_loss,
                       g_mean_loss/d_mean_loss, d_one_mean_loss, l1_mean_loss,
                       gan_mean_loss) +
                ', time = {:.1f} secs.\n'.format(epoch_time))
        f.close()
        f_time = open('../results/'+ BASE_PATH +'/time_tracker.dat', 'a')
        f_time.write('num samples: {}, epoch time: {:.5f}, training time: {:.12f}, inference time: {:.12f}\n'.
                     format(counter, epoch_time, training_time_mean, inference_time_mean))
        f_time.close()
        f_inf = open('../results/'+ BASE_PATH +'/inference_tracker.dat', 'a')
        for f_i in inf_time:
            f_inf.write('{}\n'.format(f_i))
        f_inf.close()
        # saving (checkpoint) the model every 25 epochs
        if (epoch + 1) % 25 == 0 and SAVE is True:
            print("Saving model checkpoint...")
            checkpt.write(file_prefix=checkpoint_prefix)


"""
The main controlling section runs here
"""

if TRAIN:
    train(train_input_dataset, train_truth_dataset, EPOCHS)
else:
    """## Restore the latest checkpoint and test"""
    # restoring the latest checkpoint in checkpoint_dir
    checkpt.restore(tf.train.latest_checkpoint(checkpoint_dir))

    """## Testing on the entire test dataset"""
    # Run the trained model on the entire test dataset
    zipped_test = zip(test_input_dataset, test_truth_dataset)
    for i, (inp, tar) in enumerate(zipped_test):
        P.generate_images(generator, inp, tar,
                          base_path=base_path +
                                    'esrgan/results/test',
                          epoch=i,
                          OUTPUT_CHANNELS=OUTPUT_CHANNELS)
        if i == 25:
            # only test some 25 images
            break
    print("reached the end of the images.")
