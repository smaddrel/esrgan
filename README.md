# eSRGAN

eSRGAN - electron Super Resolution Generative Adversarial Network
- Question: Can we reconstruct the original path of the electron after Coulomb scattering?

Current Functionality:
* Electron tracks generated from simulation (basic) - multiple_scattering.py
* Perturbation (Coulomb scattering) at each detector layer
    * A detector gemonetry of 4 modules, each of 4 layers, spaced over 1.5m   
    * In each layer the coulomb scattering angle and displacement is calculated and drawn from a distribution 
    * Data saved in image form, both truth - without scattering - and scattered
* Detector structure visualised

![](data/_git_images/pretty_detector.png)

* Generative network based on Pix2Pix architecture 
    * Set to be trained on GPU / locally 
    * Current tests show GPU ~ 5x faster that local MacBook CPU


Longer Term:
- Ultimately 3D, scattering, and Bremsstrahlung radiation
- Handle an arbitary number of tracks in a single image to handle pileup
- Qunatify the improvements over baseline
- Benchmarking against a Kalman Filter to be added 


# Data:
- Currently data stored as .png files from the track generation

1.   The input image, with coulomb scattering at each layer in the detector 
2.   The truth image, propagated in a straight line from initial conditions
3.   Options to apply a gaussian convolution to the image to include some resolution type effect
4.   Option to include multiple track candidates and simulate pileup 

![](data/_git_images/train_input_0.png)
![](data/_git_images/train_truth_0.png)

3. Option in track generation for N track pileup, and resolution effects


# Results:
* Three functioning examples:
    1. Track correction for a single track with gaussian convolution:
        * Bseline example, using minimal scattering (thinner detector layers)
        * Both input and traget have gaussian convolution applied 
        * Excellent recovery shown in this case: Multiple kicks understood and removed
    
    ![](data/_git_images/simple_fuzzy.png)
    ![](data/_git_images/fuzzy_big_bend.png)
    ![](data/_git_images/fuzzy_double_kink.png)

    2. Track correction for single track from smeared input to crisp output
        * More challenging example, where output is forced towards a binary on/off case for the track pixels
        * Encouraging to see runs of pixels mactching strucutre in the target, not just the direction 
    
    ![](data/_git_images/clean_line.png)
    ![](data/_git_images/clean_line_2.png)
    
    3. Multiple track candidates overlapping in the input, all corrected in a single pass without the need to segment each track individually
        * This is a complicated example, and where simpler models have struggled
        * Pix2Pix based algorithim handles the multiple candiates well
        * These are particularly challenging examples with track crossing and multiple occupancy 
        * All results look strong
    
    ![](data/_git_images/good_two_track.png)
    ![](data/_git_images/complex_two_track.png)
    ![](data/_git_images/complex_three_track.png)
    
    
# Work in progress:
* Masked input images - by training with a randomly placed rectangular mask over the input image to reflect missing / corrupt data
![](data/_git_images/seven_tracks_masked.png)

* Super Resolution
    * Trained using image pairs where the input is half the resolution of the target image
    * The work in progress here is evident from the artifacts remaining in the output images
![](data/_git_images/super_version_1.png)
![](data/_git_images/super_version_2.png)
![](data/_git_images/super_version_3.png)
![](data/_git_images/super_version_4.png)

# To run the code:
- /src/ has the code
- currently in alpha mode 
- Has a basic Pix2Pix structure, and setup to train on GPU batch system 

*  Set up python3 virtual environent with requirements 
    * `python3 -m virtualenv env_name`
    * `pip3 install -r requirements.txt`
* To train locally 
    * `python esrgan.py --train --base_path path/to/save/results/and/checkpoints --clean --one_shot`
* Options:
    * `--train` : run in training mode, defaults to false
    * `--remote` : default is false, sets the path for either local or remote and updates outputs accordingly 
    * `--base_path` : Where to save results and checkpoints, makes running multiple in parallel easier 
    * `--clean` : to clean the base_path directory or not, defulats to False
    * `--one_shot` : Turns off the one shot CNN discriminator 
    * `--lr` : Set the learning rate for the networks, default at 2e-4
    * `--n_samples` : The upper limit number of batches to run, useful for testing 
    * `--lambda`: The size of the lambda scalar that adjusts the power of the L1 loss, default is 100 
    



# Contact:
email: samuel.maddrell-mander@bristol.ac.uk
(09.11.2018)
